# node-express-server

node-express-server is a simple JS server. Developed using NodeJs & ExpressJs

## How to run

Make sure you have Node and npm installed, then run the following commands

```bash
npm run init
npm run watch:dev
```

## Usage

Hit the URL

```bash
http://localhost:3000
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.






## Note

The README file is still under construction.