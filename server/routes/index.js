import express from 'express';
import binaryEncryptUtils from '../utils/binaryEncryptUtils';

var router = express.Router();
var encryptor = new binaryEncryptUtils();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'World' });
});

router.get('/hello', (req, res, next) => {
  res.send("hello");
});

router.get('/encrypt/:number', (req, res, next) => {
  res.send(encryptor.convertAndEncrypt(2)); 
});


export default router;